package com.example.rpsapi;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class GameEntity {
    UUID uuid;
}


