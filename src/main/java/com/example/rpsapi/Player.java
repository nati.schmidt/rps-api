package com.example.rpsapi;

public class Player {

    private final String playerUsername;
    private Turn playerTurn;
 //The username cant be empty or null so it throws an exception
    Player(String name) throws IllegalArgumentException{
        if (name != null && !name.isEmpty())
            this.playerUsername = name;
        else {
            throw new IllegalArgumentException("Username cant be empty");
        }
    }

    String getPlayerUsername(){
        return playerUsername;
    }

    Turn getPlayerTurn(){
        return playerTurn;
    }
}