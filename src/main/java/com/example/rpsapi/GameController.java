package com.example.rpsapi;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/games")
@AllArgsConstructor

public class GameController {

    GameService gameService;

    private static List<Game> games = new ArrayList<>();

    @GetMapping
    public List<Game> all() {
        return gameService.all()
                .map(GameController::toDTO)
                .collect(Collectors.toList());
    }

    //testing
//    public List<Game> all() {
//        return List.of(
//                new Game(UUID.randomUUID()),
//                new Game(UUID.randomUUID())
//
//        );
//    }


    private static Game toDTO(GameEntity gameEntity) {
        return new Game(
        );
    }

//    @PostMapping("api/games/start")
//    public Game createGame(@RequestBody CreateGame createGame, Map<UUID, String> body) {
//        String playerUsername = body.get("name");
//        if (playerUsername != null && !playerUsername.isEmpty()) {
//            return toDTO(
//                    gameService.createGame(
//                            createGame.getUuid()));
//        }
//        return null;
//    }
//    @PostMapping("api/games/start")
//    public Game createGame(@RequestBody CreateGame createGame) {
//        return toDTO(
//                gameService.createGame(
//                        createGame.getUuid()));
//
//    }

//    //START GAME
//    @PostMapping("/games/start") // Create new game
//    public String createGame(@RequestBody Map<UUID, String> body) {
//        Game newGame = new Game();
//        //error here wip
//        UUID uuid = newGame.getUUID();
//        String playerUsername = body.get("name");
//
//        if (playerUsername != null && !playerUsername.isEmpty()) {
//            newGame.addPlayer(playerUsername);
//            games.add(newGame);
//            return Test.gameCreated(uuid);
//
//        }
//       return Test.errorName();
//    }

//    private Game getGame(UUID uuid) {
//        List<Game> gameList = games.stream()
//                .filter(game -> game.getUUID().equals(uuid))
//                .toList();
//        return gameList.size()  == 1 ? gameList.get(0) : null;
//                //returns first game if it exists
//     }

//    @GetMapping("/games/{uuid}") // Return game with given uuid
//    public String getGameResult(@PathVariable("uuid") UUID uuid) {
//        Game game = getGame(uuid);
//        if (game != null) {
//            return Test.gamefound(game);
//        } else {
//            return Test.gamenotfound();
//        }
//    }

}

//    public String GetToken() {
//         return UUID.randomUUID().toString();
//
//    }

//    @PostMapping
//    Game createGame(@RequestBody CreateGame createGame) {
//        return new Game(
//                createGame.getUUID(),
//                createGame.getSessionName()
//
//        );


//}
