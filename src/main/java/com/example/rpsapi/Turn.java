package com.example.rpsapi;
public enum Turn {

    ROCK {
        @Override
        public boolean wins_over(Turn opposed) {
            return opposed == SCISSOR;
        }

    },
    PAPER {
        @Override
        public boolean wins_over(Turn opposed) {
            return opposed == ROCK;
        }
    },
    SCISSOR {
        @Override
        public boolean wins_over(Turn opposed) {
            return opposed == PAPER;
        }
    };


    public abstract boolean wins_over(Turn opposed);

    public static Boolean validTurn(String turn) {
        return turn != null;
    }

}





